# Project Name: UnsplashAppDemo

It is a replica of Unsplash App

## Implementation
1) Implemented MVVM Architecture
2) Implemented SwiftLynt to follow proper design guidelines 
3) Implemented Tab Bar Controller for Home Screen, Search Screen, AddImageScreen and Profile Screen
4) Implemented Generic Network layer using URLSession to get the data from the Unsplash API 
5) Implemented NSCache library to store Images in the local cache 
